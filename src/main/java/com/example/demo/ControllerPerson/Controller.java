package com.example.demo.ControllerPerson;

import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.PushBuilder;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Address;
import com.example.demo.Model.Person;
import com.example.demo.Model.Professor;
import com.example.demo.Model.Student;
import com.example.demo.Model.Subject;

@RestController
public class Controller {
    @GetMapping("/students")
    public ArrayList<Person> getPerson() {

        ArrayList<Person> ArrayListPerson = new ArrayList<Person>();
        
        // cách 1 làm bằng arraylist
        Person person1 = new Student(2, new ArrayList<Subject>(){
            {
                add(new Subject("John" , 3 , new Professor(3 , "male" , "hải", new Address("tôn thất thuyết" , "sài gòn" , "việt nam",5000) ,3000)));
            }
        });

        Person person2 = new Student(3 , "male" , "Hải" , new Address("tôn thuyết" , "sài gòn" ,"việt nam" ,5400) , 5 , new ArrayList<Subject>(){
            {
                add(new Subject("Hóa" , 3 , new Professor(100 , "male" , "Nguyễn Hoàng Hải", new Address("Tôn Thất Thuyết" , "Sài Gòn" , "Việt Nam",5000) ,3000)));
            }
        }) ;

        // cách 2 làm bằng array 
        Student studentDiana = new Student(20, "female", "Diana", new Address(), 2, new ArrayList<Subject>(Arrays.asList(
            new Subject("Anh Văn ", 1, new Professor(50, "male", "Tom cruise", new Address() , 4000)),
            new Subject("Giải Tích ", 1, new Professor(55, "female", "Modona", new Address() , 5000))
        )));
      
       
        ArrayListPerson.add(person1);
        ArrayListPerson.add(person2);
        ArrayListPerson.add(studentDiana);

        return ArrayListPerson;

    }
}
