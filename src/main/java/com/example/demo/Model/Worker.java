package com.example.demo.Model;

public class Worker extends Person {

    private int salary ;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Worker() {
        super();
    }

    public Worker(int salary) {
        this.salary = salary;
    }
    public Worker(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }
    
    @Override
    public void eat() {
      System.out.println("Worker eating...");
        
    }
    public void Working(){
        System.out.println("Worker Working");
    }

}
